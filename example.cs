//using 'Something' Will Include a Namepsace from another chunck of code into this project 'scope'
using "System";
using "System.IO";


//A Namespace is the most basic object in C#. Above, in the 'using' statement, we ADDED the NAMESPACE 'System'
namespace MyNamespace
{
    //A Class is a generic type of object which one can inherit from(Become a child of) or call functionally(Execute functions inside it)
    class FirstClass {


        //All C# Programs must have a 'Main', this is the entry point to the program. Its a function inside the Class 'FirstClass'.
        //The most relevant thing about this is that its A. The 'Main' entry function and B. 'string[] args' is the arguments sent to this program
        // via the command line. whereas 'string[]' is a string array(could be multiple) of 'args' AKA the command line arguments
        public static void Main(string[] args) {
            
        }
    }
}